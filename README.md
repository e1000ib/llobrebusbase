# LLobrebusBase

Ejemples i idees codi llobrebus comentats a classe

## LLobrebussniffer

Codi Arduino per mostar en tot moment que s'esta enviant o revent. L'objectiu és fer un dispositiu que llegeixi tots els paquets que siguin enviats pel bus i els capturi.

## masterdevice

Codi ejemple de un simple master que envia un missatge repetidament

## dummyslave

Codi ejemple d'un dispositiu que esta sempre pendent de rebre algún missatge. No respon mai.


## libraries

Implementació en forma de llibreria del bus.
