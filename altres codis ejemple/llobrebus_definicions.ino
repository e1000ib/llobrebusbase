
//rols
enum Rol {
  master,
  slave
};

//OPCodis valids
enum OPCode {
  llegir_entrada = 1,
  llegir_sortida = 2,
  llegir_analogica = 3,
  llegir_registre = 4,
  escriure_sortida = 5,
  escriure_sortida_analogica = 6,
  escriure_registre = 7,
  error = 8 //només s'envia de l'esclau al master si hi ha hagut algu problema
};

//LLista de codis d'error possibles a retornar
enum codis_error {
  error_crc_missatge,
  operacio_no_reconeguda,
  operacio_no_definida,
  adresa_invalida,
  temps_espera_superat,
  error_process,
  error_desconegut = -1,
  no_error = 0  
};

const float SIGNALT_TIME_START = 3.5; //3.5 TB
const float SIGNALT_TIME_END = 3.5; //3.5 TB

const unsigned long TIME_OUT = 1000; //temps d'espera de resposta 1 segon
const int MAX_DATA_LENGTH = 4; //màxim número de bytes per a les dades



//structura per agrupar tots els camps duna PDU i poder separar missatges enviats de missatges rebuts

struct LLobrebus_PDU {
  const char byte_start;
  int adress;
  OPCode operacio;
  byte data[MAX_DATA_LENGTH];
  byte crc[2];
};

//Definició de taula de caracters especials:
const char char_start = 0x3A;
const char char_stop[2] = {0x0D, 0x0A};

//Definiciones de la trama llobrebus
int LONG_MAX_TRAMA = 11; // una trama llobrebus tendra màximo 11 bytes
