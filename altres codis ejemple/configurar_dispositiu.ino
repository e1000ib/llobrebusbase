
//#include <Chrono.h> //si es converteix el control de temps amb una classe

//configuracions generals dipositiu
float bps = 1; //comencem amb 1 bit x segon
float Tb()__attribute__((always_inline));
float Tb(){ return 1/bps*1000;} //fent-ho així ens oblidem de tenir que guardar-ho
int id_dispositiu; // 1 Master - resta esclaus
ROL rol_dispositiu; //sera master o slave?

//Donem un nom rellevant al pins per facilitar la lectura del codi per la placa en qüestio:
const int txp = 2;
const int txn = 3;
const int rxp = A0;
//const int rxn = A1;
const int bled = LED_BUILTIN;
//Definició temps del programa
unsigned long start_signal;
unsigned long end_signal;
