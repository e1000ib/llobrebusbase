
/* chrono library es una classe per substituri el delay sense bloquejar i a més fer-ho d'una forma pràctica a lllarg de tot el programa
 *  ja que a consequüencia del loop propi del arduino es complicat utilitzar whiles per enviar o rebre ja que l'arduino hauria d'estar
 *  podent fer altres coses. L'objectiu d'aquesta classe es donar eines per assegurar que els temps de transmisió es poden complir independentment 
 *  del loop que s'esta executat y sino declarar un error.
 */

#include "Chrono.h"

Chrono::Chrono(unsigned long t_start, unsigned long t_lap)
{
  _t_start = t_start;
  _t_lap = t_lap;
}

Chrono::Chrono(unsigned long t_start)
{
  Chrono(t_start,0);
}

Chrono::Chrono() {
  Chrono(millis(),0);  
}

void Chrono::setTStart(unsigned long t_start)
  {
    this->_t_start = t_start;
  }

unsigned long Chrono::getTStart()
  {
    return this->_t_lap;
  }

void Chrono::setTLap(unsigned long t_lap)
  {
    this->_t_lap = t_lap;
  }
  
unsigned long Chrono::getTLap()
  {
    return this->_t_lap;
  }

void Chrono::reset()
{
  _t_start = millis();
}

unsigned long Chrono::getTime(){
  return millis()-_t_start;
}

bool Chrono::isTime()
{
  return getTme()>=_t_lap;
}

bool Chrono::isTime(unsigned long time_goal)
{
  return getTime()>=time_goal;
}

void Chrono::wait()
{
  while(!isTime(_t_lap))
  {
    ;
  }
}
