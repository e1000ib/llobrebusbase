
/* chrono library es una classe per substituri el delay sense bloquejar i a més fer-ho d'una forma pràctica a lllarg de tot el programa
 *  ja que a consequüencia del loop propi del arduino es complicat utilitzar whiles per enviar o rebre ja que l'arduino hauria d'estar
 *  podent fer altres coses. L'objectiu d'aquesta classe es donar eines per assegurar que els temps de transmisió es poden complir independentment 
 *  del loop que s'esta executat y sino declarar un error.
 */

#ifndef Chrono_h
#define Chrono_h

#include  "Arduino.h"

class Chrono {
  private:
    unsigned long _t_start;
    unsigned long _t_lap; //this fixes the max desiret betwen resets;
  
  public:
    
    Chrono();
    Chrono(unsigned long t_start);
    Chrono(unsigned long t_start, unsigned long t_lap);

    //get/sets per a les varaibles internes:
    void setTStart(unsigned long t_start);

    unsigned long getTStart();

    void setTLap(unsigned long t_lap);
    
    unsigned long getTLap();
    

    //class functions
    void reset();
    unsigned long getTime();
    bool isTiem();
    bool isTime(unsigned long time_goal);
    void wait();
    //bool isExpired();
    //bool isNLap(int expected_laps);
    //bool prevent_t_surpas(unsigned long loop_time); //if the lap time remainig is leesser that the arduino loop_time provided will block execution util isExpired();
        
};

#endif
